
import useSWR from 'swr'
import { useRouter } from 'next/router'
import Image from 'next/image'

const fetcher = (...args) => fetch(...args).then((res) => res.json())

const OnePost = () => {
        
    const router = useRouter()
    
    // GET ID FORM URL
    const pid  = router.query.id
    
    const { data, error } = useSWR(`https://news-stand-server.herokuapp.com/singlepost/${pid}`, fetcher)
    if (error) return <h1 className=" text-black">Failed to load </h1>
    if (!data) return <h1 className=" text-black">Loading...</h1>


    function beTop(){
        window.scrollTo(0, 0)
      } beTop()
  return (
   <>
    {data !== undefined && (
        <div id="i" className="w-full
        bg-gradient-to-r to-blue-900 from-blue-800">
        <div className=" h-40 w-full sm:h-72 
         lg:h-72 relative">
        <Image className={` object-cover`}
        layout='fill' 
        src={data[0].img} alt='imagee' />
        </div>

        <div className=" w-full py-5 px-2">
        <h2 className=" inline font-sans text-white
        text-base font-extrabold"> {data[0].header} </h2>
        
        <div className="
        border-t-2 border-white m-5"></div>

        <p className=" px-2 text-white text-base mt-5 mb-5"> {data[0].post} </p>
        </div>
      </div>
    )}
   </>
  )
}

export default OnePost
