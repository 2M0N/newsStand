import Link from "next/link"
import Image from "next/image"
import useSWR from 'swr'

const fetcher = (...args) => fetch(...args).then((res) => res.json())

const Home = () => {
  

  const { data, error } = useSWR(`https://news-stand-server.herokuapp.com/getpost`, fetcher)
  if (error) return <h1 className=" text-black">Failed to load </h1>
  if (!data) return <h1 className=" text-black">Loading...</h1>


  console.log(data)
  return (
   <div className=" my-5
   sm:grid grid-cols-2 gap-4
   ">
   {data !== undefined && data.map((e,i)=>{
    return (
      <div key={i} className=" border-blue-900
      bg-gradient-to-r to-blue-900 from-blue-800
       border-t-4 my-5 shadow-md shadow-gray-900 w-full
       sm:my-0
       ">

        <Image className=" object-cover block
        h-48 md:h-72 w-full" 
        height={192} width="500" layout="responsive" 
        src={e.img} alt='imagee' />

        <div className=" w-full py-1 px-2 md:py-2">

        <Link href={`post/${e._id}`}>
          <a className=" inline font-sans text-white
        text-base font-extrabold md:text-xl
        "> {e.header} </a>
        </Link>
        </div>
      </div>
     
    )
   })}
   </div>
  )
}

export default Home
