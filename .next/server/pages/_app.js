"use strict";
(() => {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 127:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _app)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: external "react-icons/fa"
const fa_namespaceObject = require("react-icons/fa");
;// CONCATENATED MODULE: ./comps/Footer.jsx


const Footer = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: " bg-gradient-to-r to-blue-900 from-blue-800",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: " grid sm:grid-cols-3 h-[33vh] ",
            children: [
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: " hidden mt-5 px-3 flex pl-[30%] flex-col items-center text-white font-bold sm:block ",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("h1", {
                            className: " inline border-white border-t-4 text-2xl font-extrabold font-sans mb-2",
                            children: " Quick Links "
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                    children: " Asia"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                    children: " Middle East"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                    children: " Europe"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                    children: " Africa "
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                    children: " America "
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "flex flex-col items-center",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                            className: " text-center px-2 ring bg-blue-800 font-extrabold mt-5 py-1 ring-blue-900 rounded-md shadow-lg shadow-gray-700 text-2xl text-white font-sans",
                            children: " News-Stand"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                            className: " mt-3 flex items-center text-white font-semibold font-sans",
                            children: " Hear First, Share First!"
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: " flex justify-center text-white items-center mt-3",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaDiscord, {
                                    className: " socialIocn"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaTelegram, {
                                    className: "socialIocn"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaTwitch, {
                                    className: "socialIocn"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaYoutube, {
                                    className: "socialIocn"
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: " pl-[30%] hidden mt-5 flex-col text-white font-bold sm:block ",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("h1", {
                            className: " text-2xl font-extrabold font-sans mb-2 inline border-white border-t-4",
                            children: " Contact "
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "flex items-center text-white font-bold",
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("svg", {
                                    className: "h-5 w-5 text-white mr-2",
                                    fill: "none",
                                    viewBox: "0 0 24 24",
                                    stroke: "currentColor",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                            strokeLinecap: "round",
                                            strokeLinejoin: "round",
                                            strokeWidth: "2",
                                            d: "M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                            strokeLinecap: "round",
                                            strokeLinejoin: "round",
                                            strokeWidth: "2",
                                            d: "M15 11a3 3 0 11-6 0 3 3 0 016 0z"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    children: "Doha, 33 SA, 23 Block"
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "flex items-center",
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("svg", {
                                    className: "h-5 w-5 text-white mr-2",
                                    width: "24",
                                    height: "24",
                                    viewBox: "0 0 24 24",
                                    strokeWidth: "2",
                                    stroke: "currentColor",
                                    fill: "none",
                                    strokeLinecap: "round",
                                    strokeLinejoin: "round",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                            stroke: "none",
                                            d: "M0 0h24v24H0z"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                            d: "M5 4h4l2 5l-2.5 1.5a11 11 0 0 0 5 5l1.5 -2.5l5 2v4a2 2 0 0 1 -2 2a16 16 0 0 1 -15 -15a2 2 0 0 1 2 -2"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                            d: "M15 6l2 2l4 -4"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    children: "+223 0343434"
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "flex items-center",
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("svg", {
                                    className: "h-5 w-5 text-white mr-2",
                                    width: "24",
                                    height: "24",
                                    viewBox: "0 0 24 24",
                                    strokeWidth: "2",
                                    stroke: "currentColor",
                                    fill: "none",
                                    strokeLinecap: "round",
                                    strokeLinejoin: "round",
                                    children: [
                                        "  ",
                                        /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                            stroke: "none",
                                            d: "M0 0h24v24H0z"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                            d: "M21 14l-3 -3h-7a1 1 0 0 1 -1 -1v-6a1 1 0 0 1 1 -1h9a1 1 0 0 1 1 1v10"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                            d: "M14 15v2a1 1 0 0 1 -1 1h-7l-3 3v-10a1 1 0 0 1 1 -1h2"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    children: " Doha@mail.com.qt"
                                })
                            ]
                        })
                    ]
                })
            ]
        })
    }));
};

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(689);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
;// CONCATENATED MODULE: ./context/UC.jsx


const initialState = {
    isLoggedIn: false,
    sideBar: false,
    allPost: []
};
const Reducer = (state, action)=>{
    switch(action.type){
        case "SIDE_BAR":
            return {
                ...state,
                sideBar: !state.sideBar
            };
        case "GET_POST":
            return {
                ...state,
                allPost: state.allPost = action.payload
            };
        default:
            return state;
    }
};
const UC = /*#__PURE__*/ external_react_default().createContext();
const Provider = ({ children  })=>{
    const { 0: state , 1: dispatch  } = (0,external_react_.useReducer)(Reducer, initialState);
    return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
        children: /*#__PURE__*/ jsx_runtime_.jsx(UC.Provider, {
            value: {
                dispatch,
                sideBar: state.sideBar,
                isLoggedIn: state.isLoggedIn,
                allPost: state.allPost
            },
            children: children
        })
    }));
};
/* harmony default export */ const context_UC = (Provider);

// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(664);
;// CONCATENATED MODULE: ./comps/SideBar.jsx


const SideBar = ({ sideBarClick  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: "sidebar bg-gradient-to-t to-blue-900 from-blue-800 h-[100vh] absolute bg-blue-600 border-r-4 transition-width duration-500 border-blue-700",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaBackward, {
                className: "text-white absolute right-3 mt-2 text-lg font-extrabold",
                onClick: sideBarClick
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "flex mt-2 ml-2 items-center",
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: " flex px-2 pb-0.5 mb-4 bg-gradient-to-r to-blue-900 from-blue-800 rounded-md shadow-md shadow-gray-800 text-white font-bold ring ring-white",
                    children: " Login"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                className: " border-white border-b-4 text-lg font-bold text-white",
                children: " Sections "
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: " mt-5 py-2 px-2",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                        className: "categorySpan",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaNewspaper, {}),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: "newsCategory",
                                children: " Middle East "
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                        className: "categorySpan",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaNewspaper, {}),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: "newsCategory",
                                children: " Asia "
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                        className: "categorySpan",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaNewspaper, {}),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: "newsCategory",
                                children: " Europe "
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                        className: "categorySpan",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaNewspaper, {}),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: "newsCategory",
                                children: " Africa "
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                        className: "categorySpan",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaNewspaper, {}),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: "newsCategory",
                                children: " North America "
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                        className: "categorySpan",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaNewspaper, {}),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: "newsCategory",
                                children: " South America  "
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                        className: "categorySpan",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(fa_namespaceObject.FaNewspaper, {}),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: "newsCategory",
                                children: " South Asia "
                            })
                        ]
                    })
                ]
            })
        ]
    }));
};

;// CONCATENATED MODULE: ./comps/Header.jsx





const Header = ()=>{
    const { isLoggedIn , sideBar , dispatch  } = (0,external_react_.useContext)(UC);
    const onMenuClick = ()=>{
        dispatch({
            type: "SIDE_BAR"
        });
    };
    // Sidebar Click
    const sideBarClick = ()=>{
        dispatch({
            type: "SIDE_BAR"
        });
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            sideBar && /*#__PURE__*/ jsx_runtime_.jsx(SideBar, {
                sideBarClick: sideBarClick
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: " grid justify-center items-center bg-gradient-to-r to-blue-900 from-blue-800 h-14 w-full shadow-md shadow-gray-500",
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: " grid grid-cols-3 justify-center items-center w-screen",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: " flex items-center px-3",
                            onClick: onMenuClick,
                            children: /*#__PURE__*/ jsx_runtime_.jsx("svg", {
                                className: "svgLogo h-6 w-6 text-white ",
                                xmlns: "http://www.w3.org/2000/svg",
                                fill: "none",
                                viewBox: "0 0 24 24",
                                stroke: "currentColor",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                    strokeLinecap: "round",
                                    strokeLinejoin: "round",
                                    strokeWidth: 3,
                                    d: "M4 8h16M4 16h16"
                                })
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("h1", {
                            className: " font-sans text-center text-white text-md font-extrabold",
                            children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                href: "/",
                                children: " News-Stand "
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "flex absolute right-0 px-3 items-center",
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("svg", {
                                    className: "h-4 w-4 text-white mr-1",
                                    xmlns: "http://www.w3.org/2000/svg",
                                    viewBox: "0 0 20 20",
                                    fill: "currentColor",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                            fillRule: "evenodd",
                                            d: "M6.625 2.655A9 9 0 0119 11a1 1 0 11-2 0 7 7 0 00-9.625-6.492 1 1 0 11-.75-1.853zM4.662 4.959A1 1 0 014.75 6.37 6.97 6.97 0 003 11a1 1 0 11-2 0 8.97 8.97 0 012.25-5.953 1 1 0 011.412-.088z",
                                            clipRule: "evenodd"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                            fillRule: "evenodd",
                                            d: "M5 11a5 5 0 1110 0 1 1 0 11-2 0 3 3 0 10-6 0c0 1.677-.345 3.276-.968 4.729a1 1 0 11-1.838-.789A9.964 9.964 0 005 11zm8.921 2.012a1 1 0 01.831 1.145 19.86 19.86 0 01-.545 2.436 1 1 0 11-1.92-.558c.207-.713.371-1.445.49-2.192a1 1 0 011.144-.83z",
                                            clipRule: "evenodd"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("path", {
                                            fillRule: "evenodd",
                                            d: "M10 10a1 1 0 011 1c0 2.236-.46 4.368-1.29 6.304a1 1 0 01-1.838-.789A13.952 13.952 0 009 11a1 1 0 011-1z",
                                            clipRule: "evenodd"
                                        })
                                    ]
                                }),
                                isLoggedIn ? /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    className: " text-white text-sm font-bold font-sans ",
                                    children: " Login"
                                }) : /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    className: " text-white text-sm font-bold font-sans ",
                                    children: " Sign up"
                                })
                            ]
                        })
                    ]
                })
            })
        ]
    }));
};

;// CONCATENATED MODULE: ./comps/Layout.jsx



const Layout = ({ children  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(Header, {}),
            children,
            /*#__PURE__*/ jsx_runtime_.jsx(Footer, {})
        ]
    }));
};
/* harmony default export */ const comps_Layout = (Layout);

;// CONCATENATED MODULE: ./pages/_app.js





function MyApp({ Component , pageProps  }) {
    return(/*#__PURE__*/ jsx_runtime_.jsx(context_UC, {
        children: /*#__PURE__*/ jsx_runtime_.jsx(comps_Layout, {
            children: /*#__PURE__*/ jsx_runtime_.jsx(Component, {
                ...pageProps
            })
        })
    }));
}
/* harmony default export */ const _app = (MyApp);


/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 14:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 524:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 20:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 52:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 422:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [190,676,664], () => (__webpack_exec__(127)));
module.exports = __webpack_exports__;

})();